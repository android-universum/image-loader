Android Image Loader
===============

[![CircleCI](https://circleci.com/bb/android-universum/image-loader.svg?style=shield)](https://circleci.com/bb/android-universum/image-loader)
[![Codecov](https://codecov.io/bb/android-universum/image-loader/branch/main/graph/badge.svg)](https://codecov.io/bb/android-universum/image-loader)
[![Codacy](https://api.codacy.com/project/badge/Grade/67c575354d484b1e8a8fd1e68a02fe48)](https://www.codacy.com/app/universum-studios/image-loader?utm_source=android-universum@bitbucket.org&amp;utm_medium=referral&amp;utm_content=android-universum/image-loader&amp;utm_campaign=Badge_Grade)
[![Android](https://img.shields.io/badge/android-9.0-blue.svg)](https://developer.android.com/about/versions/pie/android-9.0)
[![Robolectric](https://img.shields.io/badge/robolectric-4.3.1-blue.svg)](http://robolectric.org)
[![Glide](https://img.shields.io/badge/glide-4.8.0-blue.svg)](https://bumptech.github.io/glide)
[![Picasso](https://img.shields.io/badge/picasso-2.71828-blue.svg)](http://square.github.io/picasso)
[![Universal Image Loader](https://img.shields.io/badge/universalimageloader-1.9.5-blue.svg)](https://github.com/nostra13/Android-Universal-Image-Loader)
[![Android Jetpack](https://img.shields.io/badge/Android-Jetpack-brightgreen.svg)](https://developer.android.com/jetpack)

## ! OBSOLETE ! ##

**> This project has become obsolete and will be no longer maintained. <**

---

Unified image loader API layer for the Android platform.

For more information please visit the **[Wiki](https://bitbucket.org/android-universum/image-loader/wiki)**.

## Download ##

Download the latest **[release](https://bitbucket.org/android-universum/image-loader/addon/pipelines/deployments "Deployments page")** or **add as dependency** in your project via:

### Gradle ###

    implementation "universum.studios.android:image-loader:${DESIRED_VERSION}@aar"

## Compatibility ##

Supported down to the **Android [API Level 14](http://developer.android.com/about/versions/android-4.0.html "See API highlights")**.

### Dependencies ###

- [`androidx.annotation:annotation`](https://developer.android.com/jetpack/androidx)
- [`androidx.legacy:legacy-support-v4`](https://developer.android.com/jetpack/androidx)

## [License](https://bitbucket.org/android-universum/image-loader/src/main/LICENSE.md) ##

**Copyright 2020 Universum Studios**

_Licensed under the Apache License, Version 2.0 (the "License");_

You may not use this file except in compliance with the License. You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software distributed under the License
is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
or implied.
     
See the License for the specific language governing permissions and limitations under the License.