Change-Log
===============
> Regular configuration update: _09.03.2020_

More **detailed changelog** for each respective version may be viewed by pressing on a desired _version's name_.

## Version 0.x ##

### [0.7.0](https://github.com/universum-studios/android_image_loader/releases/tag/0.7.0) ###
> 17.11.2018

- Regular **dependencies update** (mainly to use new artifacts from **Android Jetpack**).

### [0.6.2](https://github.com/universum-studios/android_image_loader/releases/tag/0.6.2) ###
> 06.08.2017

- **Dropped support** for _Android_ versions **below** _API Level 14_.

### [0.6.1](https://github.com/universum-studios/android_image_loader/releases/tag/0.6.1) ###
> 21.12.2016

- First pre-release.