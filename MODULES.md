Modules
===============

Library is also distributed via **separate modules** which may be downloaded as standalone parts of
the library in order to decrease dependencies count in Android projects, so only dependencies really
needed in an Android project are included. **However** some modules may depend on another modules
from this library or on modules from other libraries.

## Download ##

### Gradle ###

For **successful resolving** of artifacts for separate modules via **Gradle** add the following snippet
into **build.gradle** script of your desired Android project and use `implementation '...'` declaration
as usually.

    repositories {
        maven {
            url  "http://dl.bintray.com/universum-studios/android"
        }
    }

## Available modules ##
> Following modules are available in the [latest](https://bitbucket.org/android-universum/image-loader/downloads "Downloads page") stable release.

**[Interface](https://bitbucket.org/android-universum/image-loader/src/main/library/src/main)**

    implementation "universum.studios.android:image-loader:${DESIRED_VERSION}@aar"

**[Base](https://bitbucket.org/android-universum/image-loader/src/main/library/src/base)**

    implementation "universum.studios.android:image-loader-base:${DESIRED_VERSION}@aar"

**[Glide](https://bitbucket.org/android-universum/image-loader/src/main/library/src/glide)**

    implementation "universum.studios.android:image-loader-glide:${DESIRED_VERSION}@aar"

_depends on:_
[`com.github.bumptech.glide:glide:4.8.0`](https://github.com/bumptech/glide)

**[Picasso](https://bitbucket.org/android-universum/image-loader/src/main/library/src/picasso)**

    implementation "universum.studios.android:image-loader-picasso:${DESIRED_VERSION}@aar"

_depends on:_
[`com.squareup.picasso:picasso:2.71828`](http://square.github.io/picasso/)

**[UniversalImageLoader](https://bitbucket.org/android-universum/image-loader/src/main/library/src/universal)**

    implementation "universum.studios.android:image-loader-universal:${DESIRED_VERSION}@aar"

_depends on:_
[`com.nostra13.universalimageloader:universal-image-loader:1.9.5`](https://github.com/nostra13/Android-Universal-Image-Loader)

**[Volley](https://bitbucket.org/android-universum/image-loader/src/main/library/src/volley)**

    not avilable

_depends on:_
[`com.mcxiaoke.volley:library-aar:1.0.0`](http://developer.android.com/training/volley/index.html)

