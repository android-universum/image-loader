/*
 * *************************************************************************************************
 *                                 Copyright 2016 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.imageloader;

import android.app.Activity;
import android.content.Context;

import com.bumptech.glide.Glide;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

/**
 * Factory providing instances of {@link ImageLoader} implementations.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public abstract class ImageLoaderFactory {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "ImageLoaderFactory";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new empty instance of ImageLoaderFactory. Inheritance hierarchies should declare
	 * theirs constructors private in order to became a standard utility classes.
	 */
	protected ImageLoaderFactory() {
		// We allow to override this class only so it may be used as base for image loader factory.
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Creates a new instance of {@link ImageLoader} implementation for the given activity <var>context</var>.
	 *
	 * @param context The context for which to create the loader.
	 * @return New image loader implementation ready to be used.
	 *
	 * @see #createLoader(FragmentActivity)
	 * @see #createLoader(Fragment)
	 * @see #createLoader(Context)
	 */
	@NonNull public static ImageLoader createLoader(@NonNull final Activity context) {
		return new ImageLoaderImpl(Glide.with(context));
	}

	/**
	 * Creates a new instance of {@link ImageLoader} implementation for the given fragment activity
	 * <var>context</var>.
	 *
	 * @param context The context for which to create the loader.
	 * @return New image loader implementation ready to be used.
	 *
	 * @see #createLoader(Activity)
	 * @see #createLoader(Fragment)
	 * @see #createLoader(Context)
	 */
	@NonNull public static ImageLoader createLoader(@NonNull final FragmentActivity context) {
		return new ImageLoaderImpl(Glide.with(context));
	}

	/**
	 * Creates a new instance of {@link ImageLoader} implementation for the given fragment <var>context</var>.
	 *
	 * @param context The context for which to create the loader.
	 * @return New image loader implementation ready to be used.
	 *
	 * @see #createLoader(Activity)
	 * @see #createLoader(FragmentActivity)
	 * @see #createLoader(Context)
	 */
	@NonNull public static ImageLoader createLoader(@NonNull final Fragment context) {
		return new ImageLoaderImpl(Glide.with(context.requireActivity()));
	}

	/**
	 * Creates a new instance of {@link ImageLoader} implementation for the given <var>context</var>.
	 *
	 * @param context The context for which to create the loader.
	 * @return New image loader implementation ready to be used.
	 *
	 * @see #createLoader(Activity)
	 * @see #createLoader(FragmentActivity)
	 * @see #createLoader(Fragment)
	 */
	@NonNull public static ImageLoader createLoader(@NonNull final Context context) {
		return new ImageLoaderImpl(Glide.with(context));
	}

	/*
	 * Inner classes ===============================================================================
	 */
}