/*
 * *************************************************************************************************
 *                                 Copyright 2016 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.imageloader;

import com.bumptech.glide.RequestManager;

import androidx.annotation.NonNull;

/**
 * A {@link BaseImageLoader} implementation that wraps instance of {@link RequestManager} loader used
 * to perform loading for {@link ImageTask ImageTasks}.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
final class ImageLoaderImpl extends BaseImageLoader<RequestManager> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "ImageLoader#Glide";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of ImageLoaderImpl to wrap the given <var>loader</var>.
	 *
	 * @param loader The loader to be used to perform images loading.
	 */
	protected ImageLoaderImpl(@NonNull final RequestManager loader) {
		super(loader);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 */
	@Override public void start() {
		this.loader.onStart();
	}

	/**
	 */
	@Override public void resume() {
		start();
	}

	/**
	 */
	@Override public void pause() {
		stop();
	}

	/**
	 */
	@Override public void stop() {
		this.loader.onStop();
	}

	/**
	 */
	@Override public void destroy() {
		this.loader.onDestroy();
	}

	/**
	 */
	@Override public void onTrimMemory(final int level) {
		this.loader.onTrimMemory(level);
	}

	/**
	 */
	@Override public void onLowMemory() {
		this.loader.onLowMemory();
	}

	/*
	 * Inner classes ===============================================================================
	 */
}