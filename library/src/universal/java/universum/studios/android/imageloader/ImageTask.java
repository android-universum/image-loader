/*
 * *************************************************************************************************
 *                                 Copyright 2016 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.imageloader;

import android.graphics.Bitmap;
import android.view.View;

import com.nostra13.universalimageloader.cache.disc.DiskCache;
import com.nostra13.universalimageloader.cache.memory.MemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.display.BitmapDisplayer;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * A {@link BaseImageTask} implementation that can be used to load remote image with <b>Url</b> target.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class ImageTask extends BaseImageTask<com.nostra13.universalimageloader.core.ImageLoader, String, BitmapDisplayer> {

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "ImageTask";

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/*
	 * Constructors ================================================================================
	 */

	/*
	 * Methods =====================================================================================
	 */

	/**
	 */
	@Override protected boolean onLoad(@NonNull final com.nostra13.universalimageloader.core.ImageLoader loader, @Nullable final ImageLoader.Callback callback) {
		ensureHasTargetOrThrow();
		final DisplayImageOptions displayOptions = onPrepareDisplayOptionsBuilder(new DisplayImageOptions.Builder()).build();
		final Listener listener = callback == null ? null : new Listener(this, callback);
		if (view == null) {
			loader.loadImage(target, displayOptions, listener);
		} else {
			loader.displayImage(target, view, displayOptions, listener);
		}
		return true;
	}

	/**
	 */
	@Override @NonNull protected Bitmap onLoad(@NonNull final com.nostra13.universalimageloader.core.ImageLoader loader) {
		ensureHasTargetOrThrow();
		final DisplayImageOptions displayOptions = onPrepareDisplayOptionsBuilder(new DisplayImageOptions.Builder()).build();
		return loader.loadImageSync(target, displayOptions);
	}

	/**
	 * Prepares builder for display options with configuration based on this task's parameters.
	 *
	 * @param builder Instance of options builder to prepare.
	 * @return Prepared builder ready to build image display options to be used during loading process.
	 */
	@NonNull protected DisplayImageOptions.Builder onPrepareDisplayOptionsBuilder(@NonNull final DisplayImageOptions.Builder builder) {
		if (placeholderRes != NO_RESOURCE_ID) builder.showImageOnLoading(placeholderRes);
		if (placeholder != null) builder.showImageOnLoading(placeholder);
		if (errorRes != NO_RESOURCE_ID) builder.showImageOnFail(errorRes);
		if (error != null) builder.showImageOnFail(error);
		if (transformation != null) {
			if (!(transformation instanceof FadeInBitmapDisplayer) || !hasRequest(REQUEST_DO_NOT_ANIMATE)) {
				builder.displayer(transformation);
			}
		}
		return builder;
	}

	/**
	 */
	@Override protected boolean onRemove(@NonNull final com.nostra13.universalimageloader.core.ImageLoader loader) {
		ensureHasTargetOrThrow();
		final MemoryCache memoryCache = loader.getMemoryCache();
		final boolean removedFromMemory = memoryCache != null && memoryCache.remove(target) != null;
		final DiskCache diskCache = loader.getDiskCache();
		final boolean removedFromDisk = diskCache != null && diskCache.remove(target);
		return removedFromMemory || removedFromDisk;
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * Listener that wraps implementation of {@link ImageLoadingListener} for a specific {@link ImageLoader.Task}
	 * and {@link ImageLoader.Callback}.
	 */
	private static final class Listener implements ImageLoadingListener {

		/**
		 * Task for which has been loading performed.
		 */
		final ImageLoader.Task task;

		/**
		 * Loader callback to be invoked in case of finished loading or failed loading.
		 */
		final ImageLoader.Callback callback;

		/**
		 * Creates a new Listener wrapper for the given <var>task</var> and <var>callback</var>.
		 *
		 * @param task     The task for which has been loading performed.
		 * @param callback The loader callback to be invoked in case of finished loading or failed loading.
		 */
		Listener(final ImageLoader.Task task, final ImageLoader.Callback callback) {
			this.task = task;
			this.callback = callback;
		}

		/**
		 */
		@Override public void onLoadingStarted(@NonNull final String uri, @NonNull final View view) {
			// Ignored.
		}

		/**
		 */
		@Override public void onLoadingCancelled(@NonNull final String uri, @NonNull final View view) {
			// Ignored.
		}

		/**
		 */
		@Override public void onLoadingComplete(@NonNull final String uri, @NonNull final View view, @NonNull final Bitmap bitmap) {
			this.callback.onImageLoadFinished(task, bitmap);
		}

		/**
		 */
		@Override public void onLoadingFailed(@NonNull final String uri, @NonNull final View view, @NonNull final FailReason failReason) {
			this.callback.onImageLoadFailed(task, errorFromFailReason(failReason));
		}

		/**
		 * Creates a new instance of Error from the given <var>failReason</var>.
		 *
		 * @param failReason The reason from which to create the error.
		 * @return New error instance.
		 */
		private static ImageLoader.Error errorFromFailReason(final FailReason failReason) {
			final int reason;
			switch (failReason.getType()) {
				case IO_ERROR:
					reason = ImageLoader.Error.REASON_IO;
					break;
				case DECODING_ERROR:
					reason = ImageLoader.Error.REASON_DECODING;
					break;
				case NETWORK_DENIED:
					reason = ImageLoader.Error.REASON_NETWORK;
					break;
				case OUT_OF_MEMORY:
					reason = ImageLoader.Error.REASON_MEMORY;
					break;
				case UNKNOWN:
				default:
					reason = ImageLoader.Error.REASON_UNKNOWN;
					break;
			}
			return new ImageLoader.Error(reason, "Failed to load image.", failReason.getCause());
		}
	}
}