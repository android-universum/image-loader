/*
 * *************************************************************************************************
 *                                 Copyright 2016 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.imageloader;

import androidx.annotation.NonNull;

/**
 * Factory providing instances of {@link ImageLoader} implementations.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public abstract class ImageLoaderFactory {

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "ImageLoaderFactory";

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new empty instance of ImageLoaderFactory. Inheritance hierarchies should declare
	 * theirs constructors private in order to became a standard utility classes.
	 */
	protected ImageLoaderFactory() {
		// We allow to override this class only so it may be used as base for image loader factory.
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Provides an instance of {@link ImageLoader} implementation.
	 *
	 * @return Image loader implementation ready to be used.
	 */
	@NonNull public static ImageLoader createLoader() {
		return new ImageLoaderImpl(com.nostra13.universalimageloader.core.ImageLoader.getInstance());
	}

	/*
	 * Inner classes ===============================================================================
	 */
}