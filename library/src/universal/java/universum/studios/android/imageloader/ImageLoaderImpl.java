/*
 * *************************************************************************************************
 *                                 Copyright 2016 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.imageloader;

import android.util.Log;

import com.nostra13.universalimageloader.core.ImageLoader;

import androidx.annotation.NonNull;

/**
 * A {@link BaseImageLoader} implementation that wraps instance of {@link com.nostra13.universalimageloader.core.ImageLoader}
 * loader used to perform loading for {@link ImageTask ImageTasks}.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
final class ImageLoaderImpl extends BaseImageLoader<com.nostra13.universalimageloader.core.ImageLoader> {

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	private static final String TAG = "ImageLoader#Universal";

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of ImageLoaderImpl to wrap the given <var>loader</var>.
	 *
	 * @param loader The loader to be used to perform images loading.
	 */
	protected ImageLoaderImpl(@NonNull final ImageLoader loader) {
		super(loader);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 */
	@Override public void start() {
		resume();
	}

	/**
	 */
	@Override public void resume() {
		if (loader.isInited()) loader.resume();
	}

	/**
	 */
	@Override public void pause() {
		if (loader.isInited()) loader.pause();
	}

	/**
	 */
	@Override public void stop() {
		if (loader.isInited()) loader.stop();
	}

	/**
	 */
	@Override public void destroy() {
		if (loader.isInited()) loader.destroy();
	}

	/**
	 */
	@Override public void onLowMemory() {
		Log.w(TAG, "Running low on memory.");
	}

	/**
	 */
	@Override public void onTrimMemory(final int level) {
		Log.w(TAG, "Should trim memory for level(" + level + ").");
	}

	/*
	 * Inner classes ===============================================================================
	 */
}