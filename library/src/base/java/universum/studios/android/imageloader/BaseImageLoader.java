/*
 * *************************************************************************************************
 *                                 Copyright 2016 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.imageloader;

import android.graphics.Bitmap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Base implementation of {@link ImageLoader} that accepts implementations of{@link BaseImageTask}
 * via {@link #load(Task)}, {@link #load(Task, Callback)} and {@link #remove(Task)} methods.
 * <p>
 * This class simply wraps instance of a specific image loader and can be used as base for a custom
 * implementation of {@link ImageLoader} interface.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @param <L> Type of the loader that will be used by {@link BaseImageTask} to perform loading or
 *            removing of a specific image.
 */
public abstract class BaseImageLoader<L> implements ImageLoader {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "BaseImageLoader";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Wrapped loader used to perform image loading or removing.
	 */
	protected final L loader;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of BaseImageLoader wrapper for the given <var>loader</var> instance.
	 *
	 * @param loader The loader to be wrapped and used to perform image loading or removing process.
	 */
	protected BaseImageLoader(@NonNull final L loader) {
		this.loader = loader;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 */
	@SuppressWarnings("unchecked")
	@Override public boolean load(@NonNull final Task task, @Nullable final Callback callback) {
		if (task instanceof BaseImageTask) {
			((BaseImageTask) task).onLoad(loader, callback);
			return true;
		}
		return false;
	}

	/**
	 */
	@SuppressWarnings("unchecked")
	@Override @Nullable public Bitmap load(@NonNull final Task task) {
		return task instanceof BaseImageTask ? ((BaseImageTask) task).onLoad(loader) : null;
	}

	/**
	 */
	@SuppressWarnings("unchecked")
	@Override public boolean remove(@NonNull final Task task) {
		return task instanceof BaseImageTask && ((BaseImageTask) task).onRemove(loader);
	}

	/*
	 * Inner classes ===============================================================================
	 */
}