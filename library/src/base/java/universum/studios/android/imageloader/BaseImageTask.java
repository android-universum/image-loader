/*
 * *************************************************************************************************
 *                                 Copyright 2016 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.imageloader;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import androidx.annotation.DrawableRes;
import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Base implementation of {@link ImageLoader.Task} that implements setters for all task parameters.
 * <p>
 * This class can be used as base for a custom implementation of {@link ImageLoader.Task} interface
 * to be used in conjunction with {@link BaseImageLoader}.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @see BaseImageLoader
 *
 * @param <Loader>         Type of the specific loader that will be used by subclass of BaseImageTask
 *                         to perform image loading or removing process.
 * @param <Target>         Type of the target that is used by Task implementation to load requested
 *                         image.
 * @param <Transformation> Type of the transformation that can be applied to the loaded image.
 */
public abstract class BaseImageTask<Loader, Target, Transformation> implements ImageLoader.Task<Target, Transformation> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "BaseImageTask";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/**
	 * Request flag indicating that an attachment process of a loaded image to its associated view
	 * should not be animated.
	 * <p>
	 * <b>Request method:</b> {@link #doNotAnimate(boolean)}
	 *
	 * @see #hasRequest(int)
	 */
	protected static final int REQUEST_DO_NOT_ANIMATE = 0x00000001;

	/**
	 *
	 */
	@IntDef({REQUEST_DO_NOT_ANIMATE})
	@Retention(RetentionPolicy.SOURCE)
	protected @interface Request {}

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Target which to load.
	 */
	protected Target target;

	/**
	 * Drawable to be set to {@link #view} during loading process.
	 */
	protected Drawable placeholder;

	/**
	 * Resource id of a drawable to be set to {@link #view} during loading process.
	 */
	protected int placeholderRes = NO_RESOURCE_ID;

	/**
	 * Drawable to be set to {@link #view} when some error occurs.
	 */
	protected Drawable error;

	/**
	 * Resource id of a drawable to be set to {@link #view} when some error occurs.
	 */
	protected int errorRes = NO_RESOURCE_ID;

	/**
	 * Transformation to be applied to the loaded bitmap before it is set to {@link #view}.
	 */
	protected Transformation transformation;

	/**
	 * Set of requests for this task.
	 */
	private int requests;

	/**
	 * ImageView for which to perform loading.
	 */
	protected ImageView view;

	/*
	 * Constructors ================================================================================
	 */

	/*
	 * Methods =====================================================================================
	 */

	/**
	 */
	@Override public ImageLoader.Task<Target, Transformation> target(@NonNull final Target target) {
		this.target = target;
		return this;
	}

	/**
	 */
	@Override @NonNull public Target target() {
		return target;
	}

	/**
	 */
	@Override public ImageLoader.Task<Target, Transformation> placeholder(@DrawableRes final int resId) {
		this.placeholderRes = resId;
		this.placeholder = null;
		return this;
	}

	/**
	 */
	@Override public ImageLoader.Task<Target, Transformation> placeholder(@Nullable final Drawable placeholder) {
		this.placeholder = placeholder;
		this.placeholderRes = NO_RESOURCE_ID;
		return this;
	}

	/**
	 */
	@Override public ImageLoader.Task<Target, Transformation> error(@DrawableRes final int resId) {
		this.errorRes = resId;
		this.error = null;
		return this;
	}

	/**
	 */
	@Override public ImageLoader.Task<Target, Transformation> error(@Nullable final Drawable error) {
		this.error = error;
		this.errorRes = NO_RESOURCE_ID;
		return this;
	}

	/**
	 */
	@Override public ImageLoader.Task<Target, Transformation> transform(@Nullable final Transformation transformation) {
		this.transformation = transformation;
		return this;
	}

	/**
	 */
	@Override public ImageLoader.Task<Target, Transformation> doNotAnimate(final boolean animate) {
		this.updateRequests(REQUEST_DO_NOT_ANIMATE, animate);
		return this;
	}

	/**
	 */
	@Override public ImageLoader.Task<Target, Transformation> view(@NonNull final ImageView view) {
		this.view = view;
		return this;
	}

	/**
	 */
	@Override @NonNull public ImageView view() {
		return view;
	}

	/**
	 * Updates the current request flags.
	 *
	 * @param request  Request flag to register/unregister into/from the current request flags.
	 * @param register {@code True} to add the specified request flag into the current ones,
	 *                 {@code false} to remove it.
	 */
	private void updateRequests(final int request, final boolean register) {
		if (register) this.requests |= request;
		else this.requests &= ~request;
	}

	/**
	 * Checks whether the specified <var>request</var> flag is registered for this task or not.
	 *
	 * @param request The desired request flag to check. One of {@link #REQUEST_DO_NOT_ANIMATE}.
	 * @return {@code True} if request has been registered, {@code false} otherwise.
	 */
	protected final boolean hasRequest(@Request final int request) {
		return (requests & request) != 0;
	}

	/**
	 * Called to perform image loading process <b>asynchronously</b> via the given <var>loader</var>
	 * based on the parameters specified for this task.
	 *
	 * @param loader   The loader to be used to perform image loading.
	 * @param callback Callback to be invoked when the loading process is finished successfully or
	 *                 finished due to some failure. May be {@code null}.
	 * @return {@code True} if loading process has been initiated, {@code false} if some error has
	 * occurred.
	 */
	protected abstract boolean onLoad(@NonNull Loader loader, @Nullable ImageLoader.Callback callback);

	/**
	 * Called to perform image loading process <b>synchronously</b> via the given <var>loader</var>
	 * based on the parameters specified for this task.
	 *
	 * @param loader The loader to be used to perform image loading.
	 * @return Loaded image bitmap or {@code null} if loading process has failed.
	 * @throws ImageLoader.Error If some loader related error occurs during the loading process.
	 */
	@Nullable protected abstract Bitmap onLoad(@NonNull Loader loader);

	/**
	 * Called to perform image removing process via the given <var>loader</var> based on the parameters
	 * specified for this task.
	 *
	 * @param loader The loader to be used to perform image removing.
	 * @return {@code True} if image bitmap has been successfully removed from the cache (either
	 * memory, disk or both), {@code false} if there was no bitmap to remove.
	 */
	protected abstract boolean onRemove(@NonNull Loader loader);

	/**
	 * Ensures that this task has its <var>target</var> parameter specified.
	 * If not an {@link IllegalArgumentException} is thrown.
	 */
	protected final void ensureHasTargetOrThrow() {
		if (target == null) throw new IllegalArgumentException("No target specified");
	}

	/**
	 * Ensures that this task has its <var>view</var> parameter specified.
	 * If not an {@link IllegalArgumentException} is thrown.
	 */
	protected final void ensureHasViewOrThrow() {
		if (view == null) throw new IllegalArgumentException("No view specified.");
	}

	/*
	 * Inner classes ===============================================================================
	 */
}