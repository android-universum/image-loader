/*
 * *************************************************************************************************
 *                                 Copyright 2016 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.imageloader;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;
import com.squareup.picasso.Target;
import com.squareup.picasso.Transformation;

import java.io.IOException;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * A {@link BaseImageTask} implementation that can be used to load remote image with <b>Url</b> target.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class ImageTask extends BaseImageTask<Picasso, String, Transformation> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	private static final String TAG = "ImageTask";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/*
	 * Constructors ================================================================================
	 */

	/*
	 * Methods =====================================================================================
	 */

	/**
	 */
	@Override protected boolean onLoad(@NonNull final Picasso loader, @Nullable final ImageLoader.Callback callback) {
		ensureHasTargetOrThrow();
		final RequestCreator requestCreator = onPrepareRequestCreator(loader);
		if (view != null) requestCreator.into(view, callback != null ? new Listener(this, callback) : null);
		else requestCreator.into(new Target() {

			/**
			 */
			@Override public void onBitmapLoaded(@NonNull final Bitmap bitmap, @NonNull final Picasso.LoadedFrom from) {
				if (callback != null) callback.onImageLoadFinished(ImageTask.this, bitmap);
			}

			/**
			 */
			@Override public void onBitmapFailed(@NonNull final Exception error, @Nullable final Drawable errorDrawable) {
				if (callback != null) callback.onImageLoadFailed(
						ImageTask.this,
						new ImageLoader.Error(
								ImageLoader.Error.REASON_UNKNOWN,
								"Failed to load image.",
								error
						)
				);
			}

			/**
			 */
			@Override public void onPrepareLoad(@Nullable final Drawable placeHolderDrawable) {
				// Ignored.
			}
		});
		return true;
	}

	/**
	 */
	@Override protected Bitmap onLoad(@NonNull final Picasso loader) {
		ensureHasTargetOrThrow();
		final RequestCreator requestCreator = onPrepareRequestCreator(loader);
		try {
			return requestCreator.get();
		} catch (IOException error) {
			throw new ImageLoader.Error(
					ImageLoader.Error.REASON_IO,
					"Failed to load image.",
					error
			);
		}
	}

	/**
	 * Prepares loading request creator with configuration based on this task's parameters.
	 *
	 * @param loader Loader used to obtain the creator.
	 * @return Prepared image loading request creator to be executed.
	 */
	@NonNull protected RequestCreator onPrepareRequestCreator(@NonNull final Picasso loader) {
		final RequestCreator creator = loader.load(target);
		if (placeholderRes != NO_RESOURCE_ID) creator.error(placeholderRes);
		if (placeholder != null) creator.error(placeholder);
		if (errorRes != NO_RESOURCE_ID) creator.placeholder(errorRes);
		if (error != null) creator.placeholder(error);
		if (transformation != null) creator.transform(transformation);
		if (hasRequest(REQUEST_DO_NOT_ANIMATE)) creator.noFade();
		return creator;
	}

	/**
	 */
	@Override protected boolean onRemove(@NonNull Picasso loader) {
		throw new UnsupportedOperationException("Picasso image loader does not support removing of loaded images.");
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * Listener that wraps implementation of {@link Callback} for a specific {@link ImageLoader.Task}
	 * and {@link ImageLoader.Callback}.
	 */
	private static final class Listener implements Callback {

		/**
		 * Task for which has been loading performed.
		 */
		final ImageLoader.Task task;

		/**
		 * Loader callback to be invoked in case of finished loading or failed loading.
		 */
		final ImageLoader.Callback callback;

		/**
		 * Creates a new Listener wrapper for the given <var>task</var> and <var>callback</var>.
		 *
		 * @param task     The task for which has been loading performed.
		 * @param callback The loader callback to be invoked in case of finished loading or failed loading.
		 */
		Listener(final ImageLoader.Task task, final ImageLoader.Callback callback) {
			this.task = task;
			this.callback = callback;
		}

		/**
		 */
		@SuppressWarnings("ConstantConditions")
		@Override public void onSuccess() {
			final Drawable image = task.view().getDrawable();
			final Bitmap bitmap = image instanceof BitmapDrawable ? ((BitmapDrawable) image).getBitmap() : null;
			if (bitmap == null) {
				Log.w(TAG, "Failed to retrieve loaded bitmap from ImageView. Image drawable(" + image + ") is not instance of BitmapDrawable.");
			}
			this.callback.onImageLoadFinished(task, bitmap);
		}

		/**
		 */
		@Override public void onError(@NonNull final Exception error) {
			this.callback.onImageLoadFailed(
					task,
					new ImageLoader.Error(
							ImageLoader.Error.REASON_UNKNOWN,
							"Failed to load image.",
							error
					)
			);
		}
	}
}